const Sequelize = require("sequelize");
const Product = require("../models").product;
const { Op } = require("sequelize");

module.exports = {
  async list(req, res) {
    const { filter, page, order, category } = req.query;

    const offset = page ? parseInt(page) * 12 : 0;

    try {
      const [products, totalProducts] = await Promise.all([
        Product.findAll({
          where: {
            category: category ? parseInt(category) : { [Op.gt]: 0 },
            name: {
              [Op.like]: filter ? `%${filter.toUpperCase()}%` : "%",
            },
          },
          order: order ? [["price", order.toUpperCase()]] : [],
          offset: offset != 0 ? offset : 0,
          limit: 12,
        }),
        Product.count({
          where: {
            category: category ? parseInt(category) : { [Op.gt]: 0 },
            name: {
              [Op.like]: filter ? `%${filter.toUpperCase()}%` : "%",
            },
          },
        }),
      ]);
      console.log(totalProducts);
      const totalPages = Math.ceil(totalProducts / 12) - 1;
      const before = offset == 0 ? false : true;
      const after = offset < totalPages * 12 ? true : false;
      console.log(Math.ceil(offset / 12), totalPages);

      res.status(200).json({
        result: products,
        before,
        after,
      });
    } catch (err) {
      res.status(400).send(err);
    }
  },
};
