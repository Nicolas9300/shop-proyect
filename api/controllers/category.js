const Sequelize = require("sequelize");
const category = require("../models").category;

module.exports = {
  list(_, res) {
    return category
      .findAll({})
      .then((category) => res.status(200).send(category))
      .catch((err) => res.status(400).send(err));
  },
};
