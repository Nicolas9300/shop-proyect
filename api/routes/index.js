const productController = require("../controllers/product");
const categoryController = require("../controllers/category");
module.exports = (app) => {
  app.get("/api", (req, res) =>
    res.status(200).send({
      message:
        "Example project did not give you access to the api web services",
    })
  );
  app.get("/api/product/list", productController.list);
  app.get("/api/product/category/list", categoryController.list);
};
