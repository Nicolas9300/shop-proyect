import api from "./bsale_api.js";

export function paginacion() {
  const d = document,
    $after = d.getElementById("after"),
    $before = d.getElementById("before");

  $after.addEventListener("click", () => {
    api.page = api.page + 1;
    location.hash = `#/search?page=${api.page}`;
  });

  $before.addEventListener("click", () => {
    api.page = api.page - 1;
    location.hash = `#/search?page=${api.page}`;
  });
}
