export function ProductCard(props) {
  let { name, url_image, price } = props;

  let urlImg = url_image ? url_image : "app/assets/Dog.png";

  return `
    <div class="prod-card">
     <img src=${urlImg} alt="${name}">
     <h2>${name}</h2>
     <div>
       <span>$${price}</span>
       <button>Comprar</button>
     </div>
    </div>
  `;
}
