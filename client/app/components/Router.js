import { ajax } from "../helpers/ajax.js";
import api from "../helpers/bsale_api.js";
import { ProductCard } from "./ProductCard.js";

export async function Router() {
  const d = document,
    w = window,
    $main = d.getElementById("main"),
    $span = d.getElementById("spanCategory"),
    $after = d.getElementById("after"),
    $before = d.getElementById("before");

  let category = localStorage.getItem("bsCategory")
    ? localStorage.getItem("bsCategory")
    : "";
  $span.textContent = category;

  let { hash } = location;

  if (!hash || hash === "#/") {
    localStorage.removeItem("bsCategory");
    localStorage.removeItem("bsCategoryId");
    localStorage.removeItem("bsSearch");
    localStorage.removeItem("bsOrder");

    api.page = 0;

    let category = localStorage.getItem("bsCategory")
      ? localStorage.getItem("bsCategory")
      : "";
    $span.textContent = category;

    await ajax({
      url: api.SITE,
      cbSuccess: (products) => {
        console.log(products);
        let html = "";
        products.result.forEach((product) => (html += ProductCard(product)));
        $main.innerHTML = html;

        if (!products.after) {
          $after.disabled = true;
        } else {
          $after.disabled = false;
        }
        if (!products.before) {
          $before.disabled = true;
        } else {
          $before.disabled = false;
        }
      },
    });
  } else if (hash.includes("#/search")) {
    let filter = localStorage.getItem("bsSearch")
      ? localStorage.getItem("bsSearch")
      : "";
    let order = localStorage.getItem("bsOrder")
      ? localStorage.getItem("bsOrder")
      : "";
    let category = localStorage.getItem("bsCategoryId")
      ? localStorage.getItem("bsCategoryId")
      : "";

    await ajax({
      url: `${api.SITE}?filter=${filter}&category=${category}&order=${order}&page=${api.page}`,
      cbSuccess: (products) => {
        let html = "";

        if (products.result.length === 0) {
          html = `
            <p class="error">No existen resultados de busqueda para ${filter}</p>
          `;
        }
        products.result.forEach((product) => (html += ProductCard(product)));
        $main.innerHTML = html;

        if (!products.after) {
          $after.disabled = true;
        } else {
          $after.disabled = false;
        }
        if (!products.before) {
          $before.disabled = true;
        } else {
          $before.disabled = false;
        }
      },
    });
  }

  await ajax({
    url: api.CATEGORIES,
    cbSuccess: (categories) => {
      let html = `<option value="">Elige una Categoria</option>`;
      categories.forEach(
        (category) =>
          (html += `<option value="${category.id}" class="option">${category.name}</option>`)
      );
      d.getElementById("categories").innerHTML = html;
    },
  });

  d.querySelector(".loader").style.display = "none";
}
