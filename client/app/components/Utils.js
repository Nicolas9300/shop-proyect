import api from "../helpers/bsale_api.js";

export function Utils(props) {
  //let { category } = props;
  const $div = document.createElement("div");
  const $span = document.createElement("span");
  const $nav = document.createElement("nav");

  $div.classList.add("utils");

  $span.id = "spanCategory";
  $nav.innerHTML = `
  <button id="before" ><</button>
  <button id="after" >></button>  
  `;

  $div.appendChild($span);
  $div.appendChild($nav);
  return $div;
}
