import api from "../helpers/bsale_api.js";

export function SearchForm() {
  const d = document,
    $form = d.createElement("form"),
    $input = d.createElement("input");

  $form.classList.add("form-search");
  $input.name = "search";
  $input.type = "search";
  $input.placeholder = "Buscar productos...";
  $input.autocomplete = "off";

  $form.appendChild($input);

  if (location.hash.includes("#/search")) {
    $input.value = localStorage.getItem("bsSearch");
  }

  d.addEventListener("search", (e) => {
    if (!e.target.matches("input[type = 'search']")) return false;
    if (!e.target.value) localStorage.removeItem("bsSearch");
  });

  d.addEventListener("submit", (e) => {
    if (!e.target.matches(".form-search")) return false;

    e.preventDefault();

    api.page = 0;
    localStorage.setItem("bsSearch", e.target.search.value);
    location.hash = `#/search?search=${e.target.search.value}`;
  });

  return $form;
}
