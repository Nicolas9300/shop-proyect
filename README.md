# API Shop

La pagina consulta la api para renderizar los productos en el main.
Tiene un buscador, filtro para ordenarlos por categoria y tambien para ordernar del 
menor precio al mayor o de mayor a menor. Tambien tiene implementado la paginacion
en donde cargaran los siguientes productos de la busqueda deseada.

**Deploys:**
- [Backend Heroku](https://bshop-nico.herokuapp.com/api/product/list)
- [Frontend Netlify](https://shop-nico9300.netlify.app/)

## Estructuras JSON
Cuando hagas una peticion HTTP para productos, el json devuelto va a ser el siguiente:

```json 
{
    "result":[],
    "before": boolean,
    "after": boolean
}

```
 - *result* es donde se almacenan todos los productos devueltos
 - *before* devuelve un boolean indicando si tiene una lista de 12 productos antes que este
 - *after* devuelve un boolean indicando si tiene una lista de 12 productos despues que este

### GET lista de productos

- ``` GET  /api/product/list ``` devuelve lista de productos con una limitacion de 12 productos

**Parámetros**
- *filter*, filtra por la palabra mandada
- *order*, orderna de manera ASC o DESC el precio de los productos devueltos
- *page*, hace un offset hacia los siguientes o anteriores 12 resultados
- *category* , devuelve los productos filtrados por id de categoria

**Ejemplo**

 - ``` GET  /api/product/list?filter=energetica&order=asc ```
**Resultado**
```json
{
    "result": [
        {
            "id": 35,
            "name": "ENERGETICA MAKKA DRINKS",
            "url_image": "https://dojiw2m9tvv09.cloudfront.net/11132/product/makka-drinks-250ml0455.jpg",
            "price": 1190,
            "discount": 0,
            "category": 1
        },
        {
            "id": 7,
            "name": "ENERGETICA SCORE",
            "url_image": "https://dojiw2m9tvv09.cloudfront.net/11132/product/logo7698.png",
            "price": 1290,
            "discount": 0,
            "category": 1
        },
        {
            "id": 5,
            "name": "ENERGETICA MR BIG",
            "url_image": "https://dojiw2m9tvv09.cloudfront.net/11132/product/misterbig3308256.jpg",
            "price": 1490,
            "discount": 20,
            "category": 1
        },
        {
            "id": 6,
            "name": "ENERGETICA RED BULL",
            "url_image": "https://dojiw2m9tvv09.cloudfront.net/11132/product/redbull8381.jpg",
            "price": 1490,
            "discount": 0,
            "category": 1
        },
        {
            "id": 34,
            "name": "ENERGETICA MONSTER RIPPER",
            "url_image": "https://dojiw2m9tvv09.cloudfront.net/11132/product/mosterriper0436.jpg",
            "price": 1990,
            "discount": 0,
            "category": 1
        },
        {
            "id": 36,
            "name": "ENERGETICA MONSTER VERDE",
            "url_image": "https://dojiw2m9tvv09.cloudfront.net/11132/product/monsterverde0476.jpg",
            "price": 1990,
            "discount": 0,
            "category": 1
        },
        {
            "id": 77,
            "name": "ENERGETICA MONSTER RIPPER",
            "url_image": "",
            "price": 1990,
            "discount": 0,
            "category": 1
        },
        {
            "id": 79,
            "name": "ENERGETICA MONSTER VERDE",
            "url_image": "",
            "price": 1990,
            "discount": 0,
            "category": 1
        }
    ],
    "before": false,
    "after": false
}
```

### GET Categorias

- ```GET /api/product/category/list``` devuelve una lista con las categorias y sus respectivos id

**Resultado**

```json
[
    {
        "id": 1,
        "name": "bebida energetica"
    },
    {
        "id": 2,
        "name": "pisco"
    },
    {
        "id": 3,
        "name": "ron"
    },
    {
        "id": 4,
        "name": "bebida"
    },
    {
        "id": 5,
        "name": "snack"
    },
    {
        "id": 6,
        "name": "cerveza"
    },
    {
        "id": 7,
        "name": "vodka"
    }
]
```
